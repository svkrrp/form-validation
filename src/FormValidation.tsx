import {useState, useEffect} from 'react';
import FormValidator from "./FormValidator";

export const useValidatedState = (formValidator: any, variableName: any, initValue: any) => {
  const [state, setState] = useState(initValue);
  
  useEffect(() => {
    formValidator.validate({ [variableName]: state });
  }, [state]);

  return [state, setState];
}

export const withFormValidation = (WrappedComponent: any, schema: any) => {
  return (props: any) => {
    const formValidator = new FormValidator(schema);
    return (<WrappedComponent formValidator={formValidator} {...props}/>)
  }
}
