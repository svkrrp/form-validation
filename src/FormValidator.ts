export default class {
  schema: any;
  response: any;
  constructor(schema: any) {
    this.schema = schema;
    this.response = Object.keys(this.schema).reduce((result, currentKey) => ({...result, [currentKey]: false}), {});
  }

  validate(values: any) {
    Object.keys(values).forEach((key) => {
      switch (this.schema[key].type) {
        case 'regex':
          this.response[key] = this.schema[key].validation.test(values[key]);
          break;
        case 'custom':
          this.response[key] = this.schema[key].validation(values[key]);
          break;
        default:
          break;
      }
    })
  }
}
