import {useEffect, useState} from "react";
import { useValidatedState, withFormValidation } from './FormValidation';

const schema = {
  name: {
    type: 'regex',
    validation: /^[a-zA-Z ]+$/,
  },
  password: {
    type: 'regex',
    validation: /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
  }
};

function App({ formValidator }: any) {
  const [name, setName] = useValidatedState(formValidator, 'name', '');
  const [password, setPassword] = useValidatedState(formValidator, 'password', '');
  const [isDisabled, setIsDisabled] = useState(true);

  useEffect(() => {
    setIsDisabled(!Object.keys(formValidator.response).reduce((res, current) => res && formValidator.response[current], true));
  }, [name, password]);

  const onSubmit = (e: any) => {
    e.preventDefault();
    console.log("Form Submit", name, password);
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input id='name' type="text" onChange={(e) => setName(e.target.value)} placeholder="Enter name" />
        <input id='password' type="password" onChange={(e) => setPassword(e.target.value)} placeholder="Enter password" />
        <button onClick={onSubmit} disabled={isDisabled}>Submit</button>
      </form>
    </div>
  );
}

export default withFormValidation(App, schema);
